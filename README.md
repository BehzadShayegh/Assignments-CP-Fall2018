# HomeWorks-ComputerProgrammFall98ing
Computer Programming - UT ES - Fall 2018 - As TA

Here is a collection of answers of simple home works for computer programming course (fall 2018). we were Teacher Assistant of course.
you can find problems discriptions and challenges in https://www.hackerrank.com/contests/c971/ .

![Alt text](./readme.PNG?raw=true "Game Environment")

Contributors :
  - Behzad Shayegh
  - Moein Verkiani
  - Mohammad Hadi Masoumi
