#include <iostream>

using namespace std;
int binary_search(float numbers[], int start, int end, float number)
{
    if (end < start)
        return -1;
    int mid = (start + end) / 2;

    if (numbers[mid] > number)
        return binary_search(numbers, start, mid - 1, number);
    else if (numbers[mid] < number)
        return binary_search(numbers, mid + 1, end, number);
    else
        return mid;


}

int main()
{
    float numbers[20] = { -100,-20,-10.5,-2,0,2,7,15,18,20,30,35,40,41,45,50,51,55,101,110 };

    float number;
    cin >> number;

    int result = binary_search(numbers, 0, 19, number);

    if (result == -1)
        cout << "The number " << number << " was not found.";
    else
        cout << "The number " << number << " was found in cell with index: " << result;


    return 0;
}