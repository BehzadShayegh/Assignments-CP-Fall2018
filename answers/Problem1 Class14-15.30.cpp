#include <iostream>
using namespace std;
int main(){
    int a;
    int number[6]={0};
    while(cin >> a){
        if(a==0) break;
        if(a!=1 && a!=2 && a!=3 && a!= 4 && a!=5){
            cout << "Wrong Input!" << endl;
            continue;
        }

        number[a]++;
    }

    for(int i=1; i<6; i++)
        cout << i << " " << number[i] << endl;
}
